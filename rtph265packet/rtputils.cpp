#include "utils.h"

int16_t reverse16(int16_t src){
	return (src >> 8) | (src << 8);
}

uint32_t reverse32(uint32_t src){
	int16_t high = reverse16(src & 0xffff);
	int16_t low = reverse16(src >> 16);
	return (high << 16) | low;
}