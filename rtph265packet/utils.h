#ifndef __UTILS_H__
#define __UTILS_H__
#include "types.h"
int16_t reverse16(int16_t src);
uint32_t reverse32(uint32_t src);
#endif