#include "udpsend.h"
#include "h265rtpenc.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
int udpsend_init(udpsend_ctx_t* udpsend_ctx){
	udpsend_ctx->sock_Client = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);//创建客户端用于通信的Socket
	udpsend_ctx->addr_sendto.sin_family = AF_INET;
	udpsend_ctx->addr_sendto.sin_port = htons(4567);//端口号为4567
	udpsend_ctx->addr_sendto.sin_addr.s_addr = inet_addr("192.168.0.106");   //127.0.0.1为本电脑IP地址
	return 0;
}


int  upd_rtpsend(h265_rtpenc_ctx_t* h265_rtpenc_ctx){
	int ret;
	udpsend_ctx_t* udpsend_ctx = (udpsend_ctx_t*)h265_rtpenc_ctx->privatedata;
	ret = sendto(udpsend_ctx->sock_Client, (const char*)h265_rtpenc_ctx->packet_buffer, h265_rtpenc_ctx->packetlen, 0, (struct sockaddr*)&udpsend_ctx->addr_sendto, sizeof(sockaddr));
	usleep(20000);
	return 0;
}


int udpsend_close(udpsend_ctx_t* udpsend_ctx){
	close(udpsend_ctx->sock_Client);
	return  0;
}