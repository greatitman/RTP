#ifndef H265_RTPENC_H_
#define H265_RTPENC_H_
#include "types.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum rtpenc_type_t{
	RTP_ENC_H264 = 0,
	RTP_ENC_H265,

	RTP_DEC_H264,
	RTP_DEC_H265,
};

enum FrmFlag_t{//标志是最后一个包.
	StartFRM = 8, //一帧的第一个包
	EndFRM = 10, //一帧的最后一个包	
	EndFRM_NONE, //未知
};

struct rtp_pack_t {
	char *buff;
	int32_t  buffsize;
	int32_t  datalen;

	uint32_t pkt_ts;
	FrmFlag_t frmflag;
	uint16_t pkt_no;

	rtp_pack_t(int32_t bufferSize = 0) {
		if (bufferSize > 0) {
			buff = new char[bufferSize];
			buffsize = bufferSize;
			frmflag = EndFRM_NONE;
		}
		else {
			buff = NULL;
			buffsize = 0;
			frmflag = EndFRM_NONE;
		}
	}
	rtp_pack_t(const rtp_pack_t& rtp_ptk) {
		buff = rtp_ptk.buff;
		buffsize = rtp_ptk.buffsize;
		pkt_ts = rtp_ptk.pkt_ts;
		pkt_no = rtp_ptk.pkt_no;
		frmflag = rtp_ptk.frmflag;
		datalen = rtp_ptk.datalen;
	}
	void free(){
		if (buff != NULL) {
			delete[]buff;
			buff = NULL;
		}
	}
	int32_t checkbytes(char* subbytes, int32_t sublens) {

		int32_t i, j;
		for (i = 0; i <= datalen - sublens; i++) {
			for (j = 0; j < sublens; j++) {
				if (subbytes[j] != buff[i + j]) {
					break;
				}
			}

			if (j == sublens) {
				return 0;
			}
		}

		return -1;
	}
};

typedef struct rtp_header_t {
	/* byte 0 */
#ifdef WORDS_BIGENDIAN
	unsigned char version : 2;
	unsigned char padding : 1;
	unsigned char extension : 1;
	unsigned char csrc_len : 4;
#else
	unsigned char csrc_len : 4;
	unsigned char extension : 1;
	unsigned char padding : 1;
	unsigned char version : 2;
#endif
	/* byte 1 */
#if WORDS_BIGENDIAN
	unsigned char marker : 1;
	unsigned char payload : 7;
#else
	unsigned char payload : 7;
	unsigned char marker : 1;
#endif
	/* bytes 2, 3 */
	uint16_t seq_no;
	/* bytes 4-7 */
	uint32_t timestamp;
	/* bytes 8-11 */
	uint32_t ssrc;					/* stream number is used here. */
} rtp_header_t;
typedef struct h265_rtpenc_ctx_t{
	int startcodeLen;
	uint32_t timestampinc;
	char useCustomts;
	uint32_t  timestamp;
	uint16_t seq;
	uint32_t ssrc;
	int64_t vpkt_count;
	int mtusize;
	uint8_t * nalu_buffer;
	uint8_t * packet_buffer;
	int nalu_size;
	int64_t sendtotalbytes;
	//set by caller,the last nalu in the frame
	char blastnal;
	int packetlen;
	char hasvpsspspps;
	void *privatedata;
}h265_rtpenc_ctx_t;

int h265_rtpenc_init(h265_rtpenc_ctx_t *h265_rtpenc_ctx);
int h265_rtpenc(h265_rtpenc_ctx_t *h265_rtpenc_ctx);
int h265_rtpenc_uninit(h265_rtpenc_ctx_t *h265_rtpenc_ctx);
#endif