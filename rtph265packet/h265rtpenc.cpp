#include "h265rtpenc.h"
#include "udpsend.h"
int getstart_code(h265_rtpenc_ctx_t* h265_rtpenc_ctx) {
	uint8_t *inbuf = h265_rtpenc_ctx->nalu_buffer;
	if ((inbuf[0] == 0x00) && (inbuf[1] == 0x00) && (inbuf[2] == 0x00) && (inbuf[3] == 0x01)) {
		h265_rtpenc_ctx->startcodeLen = 4;
		return 4;
	}
	else if ((inbuf[0] == 0x00) && (inbuf[1] == 0x00) && (inbuf[2] == 0x01)) {
		h265_rtpenc_ctx->startcodeLen = 3;
		return 3;
	}

	return 0;
}
int h265_rtpenc_init(h265_rtpenc_ctx_t *h265_rtpenc_ctx){
	h265_rtpenc_ctx->seq = 0;
	h265_rtpenc_ctx->timestamp = 0;
	h265_rtpenc_ctx->timestampinc = 3600;
	h265_rtpenc_ctx->ssrc = 0x123456;
	h265_rtpenc_ctx->vpkt_count = 0;
	h265_rtpenc_ctx->sendtotalbytes = 0;
	h265_rtpenc_ctx->mtusize = 1350;
	h265_rtpenc_ctx->hasvpsspspps = 1;
	h265_rtpenc_ctx->packet_buffer = (uint8_t *)malloc(h265_rtpenc_ctx->mtusize);
	if (h265_rtpenc_ctx->packet_buffer == NULL)
	{
		return -1;
	}
	return 0;
}
int h265_rtpenc_uninit(h265_rtpenc_ctx_t *h265_rtpenc_ctx){
	if (h265_rtpenc_ctx->packet_buffer != NULL)
	{
		free(h265_rtpenc_ctx->packet_buffer);
	}
	return 0;
}
int h265_rtpenc_header(h265_rtpenc_ctx_t *h265_rtpenc_ctx, rtp_header_t *r) {
	r->version = 2;
	r->padding = 0;
	r->extension = 0;
	r->csrc_len = 0;
	r->marker = 0;
	r->payload = 96;
	r->seq_no = reverse16(h265_rtpenc_ctx->seq);
	h265_rtpenc_ctx->seq += 1;
	r->timestamp = reverse32(h265_rtpenc_ctx->timestamp);
	if (!h265_rtpenc_ctx->useCustomts &&h265_rtpenc_ctx->blastnal)
	{
		h265_rtpenc_ctx->timestamp += h265_rtpenc_ctx->timestampinc/*3600*/;
	}
	r->ssrc = reverse32(h265_rtpenc_ctx->ssrc);
	return 0;
}

static int h265_rtpenc_packets(h265_rtpenc_ctx_t *h265_rtpenc_ctx) {
	uint8_t *inbuffer = h265_rtpenc_ctx->nalu_buffer;
	int32_t nalu_size = h265_rtpenc_ctx->nalu_size;
	int32_t i = 0;
	rtp_header_t rtpheader;

	inbuffer += h265_rtpenc_ctx->startcodeLen;
	nalu_size -= h265_rtpenc_ctx->startcodeLen;

	h265_rtpenc_header(h265_rtpenc_ctx, &rtpheader);
	uint8_t nal_type = (inbuffer[0] >> 1) & 0x3F;

	if ((sizeof(rtp_header_t)+nalu_size) <= h265_rtpenc_ctx->mtusize) {
		rtpheader.marker = h265_rtpenc_ctx->blastnal;

		memcpy(h265_rtpenc_ctx->packet_buffer, &rtpheader, sizeof(rtpheader));
		memcpy(h265_rtpenc_ctx->packet_buffer + sizeof(rtp_header_t), inbuffer, (nalu_size));
		h265_rtpenc_ctx->packetlen = (nalu_size)+sizeof(rtp_header_t);
		h265_rtpenc_ctx->vpkt_count++;//统计包数目给rtcp发送sr用
		upd_rtpsend(h265_rtpenc_ctx);
		h265_rtpenc_ctx->sendtotalbytes += h265_rtpenc_ctx->packetlen;

		//printf("rtp------:%d, %d, %d  (%x %x)\n",htonl(r.timestamp),ntohs(r.seq_no),r.marker,mtu_buffer[0],mtu_buffer[1]);
	}
	else{
		inbuffer += 2;
		nalu_size -= 2;
		int32_t left = nalu_size;
		for (i = 0; i < (nalu_size);) {
			int32_t sendlen = h265_rtpenc_ctx->mtusize - sizeof(rtp_header_t)-2 - 1;
			if (left < sendlen) {
				sendlen = left;
			}
			memcpy(h265_rtpenc_ctx->packet_buffer + sizeof(rtp_header_t)+2 + 1, inbuffer, sendlen);
			inbuffer += sendlen;


			h265_rtpenc_ctx->packet_buffer[sizeof(rtp_header_t)] = (49 << 1);
			h265_rtpenc_ctx->packet_buffer[sizeof(rtp_header_t)+1] = 1;
			h265_rtpenc_ctx->packet_buffer[sizeof(rtp_header_t)+2] = nal_type;
			if (i == 0){
				h265_rtpenc_ctx->packet_buffer[sizeof(rtp_header_t)+2] |= 0x80;
				rtpheader.marker = 0;
			}
			else if (i + sendlen >= (nalu_size)) {
				h265_rtpenc_ctx->packet_buffer[sizeof(rtp_header_t)+2] |= 0x40;
				rtpheader.marker = h265_rtpenc_ctx->blastnal;
			}
			else{
				rtpheader.marker = 0;
			}
			memcpy(h265_rtpenc_ctx->packet_buffer, &rtpheader, sizeof(rtpheader));

			i += sendlen;
			left -= sendlen;

			h265_rtpenc_ctx->packetlen = sendlen + (sizeof(rtp_header_t)+2 + 1);
			h265_rtpenc_ctx->vpkt_count++;//统计包数目给rtcp发送sr用
			upd_rtpsend(h265_rtpenc_ctx);
			h265_rtpenc_ctx->sendtotalbytes += h265_rtpenc_ctx->packetlen;
			//printf("rtp------:%d, %d, %d  (%x %x)\n",htonl(r.timestamp),ntohs(r.seq_no),r.marker,mtu_buffer[0],mtu_buffer[1]);
		}
	}

	return 0;
}

int h265_rtpenc(h265_rtpenc_ctx_t *h265_rtpenc_ctx) {
	int ret;
	int32_t vlaue = 0;
	if (!h265_rtpenc_ctx->nalu_buffer)
		return -1;

	vlaue = getstart_code(h265_rtpenc_ctx);
	if (vlaue) {
		ret = h265_rtpenc_packets(h265_rtpenc_ctx);
	}
	else{
		printf("cannot find startcode in this nalu\n");
	}

	return 0;
}