#ifndef UDPSEND_H__
#define UDPSEND_H__

#include "h265rtpenc.h"
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;


#define BUFFER_SIZE  1024    //缓冲区大小
typedef struct udpsend_ctx_t{
	int sock_Client; //客户端用于通信的Socket
	struct sockaddr_in addr_sendto;   //地址数据结构
}udpsend_ctx_t;
int udpsend_init(udpsend_ctx_t* udpsend_ctx);
int  upd_rtpsend(h265_rtpenc_ctx_t* h265_rtpenc_ctx);
int udpsend_close(udpsend_ctx_t* udpsend_ctx);

#endif