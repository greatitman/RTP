#include <stdlib.h>
#include <stdio.h>
#include "typedef.h"
#include <string.h>
#include "flv.h"
#include "flv_bytestream.h"
#define MAX_RTP_TRAN_SIZE 		1200
#define MAX_VIDEO_FRAME_SIZE		409600
#define MAX_AUDIO_FRAME_SIZE		102400




typedef struct packet_t{
	unsigned char *packetbuf;
	int packetsize;
	int packetcap;
}packet_t;

typedef struct _rtp_header_t
{
	uint8_t i_version;
	uint8_t i_extend;
	uint8_t i_m_tag;
	uint8_t i_cc;
	uint8_t i_pt;
	uint32_t i_seq_num;
	uint32_t i_timestamp;
	uint32_t i_ssrc;
	uint32_t i_csrc;

	uint8_t i_nalu_header;
} rtp_header_t;

int RtpTo264(unsigned char* buffer, int recv_bytes, char* save_buffer, rtp2flvCtx_t *p_rtp,int payloadoffset);
int get_rtp_header(rtp_header_t* p_header, uint8_t* p_buf, uint32_t i_size);
int rtp2flv(FILE* rtp_fd, char *flv_fd, unsigned int rtptimebase);