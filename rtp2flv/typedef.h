#ifndef __TYPE_H__
#define __TYPE_H__

#define true 1
#define false 0
typedef unsigned char _bool;


typedef unsigned short uint16_t;
typedef unsigned char uint8_t;

typedef unsigned int uint32_t;

//typedef long long  int64_t;
typedef int int32_t;
typedef short int16_t;
//typedef char int8_t;

typedef unsigned long long int uint64_t;
typedef unsigned int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef  int int32;
typedef  short int16;
typedef  char int8;


typedef long off_t;
/*
static inline uint32_t endian_fix32( uint32_t x )
{
return (x<<24) + ((x<<8)&0xff0000) + ((x>>8)&0xff00) + (x>>24);
}

static inline uint64_t endian_fix64( uint64_t x )
{
return endian_fix32(x>>32) + ((uint64_t)endian_fix32(x)<<32);
}
*/
#endif