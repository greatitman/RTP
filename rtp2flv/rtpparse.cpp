#include "rtp2flv.h"

int get_rtp_header(rtp_header_t* p_header, uint8_t* p_buf, uint32_t i_size)
{
	int i_ret = 0;
	int payloadoffset = 12;
	if (p_header == NULL || p_buf == NULL || i_size < 12)
	{
		i_ret = -1;
	}
	else
	{
		p_header->i_version = (p_buf[0] & 0xC0) >> 6;
		p_header->i_extend = (p_buf[0] & 0x10) >> 4;
		p_header->i_cc = (p_buf[0] & 0x0F);
		p_header->i_m_tag = (p_buf[1] & 0x80) >> 7;
		p_header->i_pt = (p_buf[1] & 0x7F);
		p_header->i_seq_num = (p_buf[2] << 8);
		p_header->i_seq_num += p_buf[3];
		p_header->i_timestamp = (p_buf[4] << 24);
		p_header->i_timestamp += (p_buf[5] << 16);
		p_header->i_timestamp += (p_buf[6] << 8);
		p_header->i_timestamp += p_buf[7];

		p_header->i_ssrc = (p_buf[8] << 24);
		p_header->i_ssrc += (p_buf[9] << 16);
		p_header->i_ssrc += (p_buf[10] << 8);
		p_header->i_ssrc += p_buf[11];

		//p_header->i_csrc = (p_buf[12] << 24);
		//p_header->i_csrc += (p_buf[13] << 16);
		//p_header->i_csrc += (p_buf[14] << 8);
		//p_header->i_csrc += p_buf[15];
		if (p_header->i_cc ){
			if (i_size<(12 + 4 * p_header->i_cc))
			{
				printf("i_cc header outof range,packet size =%d\n", i_size);
				return -1;
			}
			payloadoffset += 4* p_header->i_cc;
			printf("cc size %d\n",4* p_header->i_cc);
		}
		if (p_header->i_extend){
			if (i_size < 14)
			{
				printf("extend header outof range,packet size =%d\n", i_size);
				return -1;
			}
			int extendsize = 4*(p_buf[payloadoffset + 2]<<8 | p_buf[payloadoffset + 3]);
			printf("extend header size %d\n",extendsize);
			payloadoffset += 4 + extendsize;
		}
	
		return payloadoffset;
	}
	return i_ret;
}

//buffer:接收到的数据；recv_bytes数据长度
//int RtpTo264(unsigned char* buffer, int recv_bytes, unsigned char* save_buffer, int* pnNALUOkFlag, int* pnLastPkt)

int RtpTo264(unsigned char* buffer, int recv_bytes, char* save_buffer, rtp2flvCtx_t *p_rtp,int payloadoffset)
{
	unsigned int FU_FLAG = 0;
	unsigned int MARK_BIT = 0;
	unsigned char NAL_HEAD = 0;
	int save_len = 0;
	unsigned int nPkt = 0;
	if (recv_bytes < payloadoffset +2)
	{
		printf("data error");
		return -1;
	}
	nPkt = (unsigned int)(((buffer[payloadoffset + 2]) << 8) | (buffer[payloadoffset+3]));
	if (nPkt - (p_rtp->i_last_pkt_num) > 1)
	{
		//printf("rtp lose packet, nPkt = %u, last = %u\n", nPkt, *pnLastPkt);//掉包。
		p_rtp->i_nalu_ok_flag = 0;
	}
	if (nPkt < (p_rtp->i_last_pkt_num))
	{
		//跳变
		//printf("rtp lose packet 2\n");
	}
	(p_rtp->i_last_pkt_num) = nPkt;
	FU_FLAG = (buffer[payloadoffset])&(0x1C); //第13个字节和0x1C相与

	//printf("%x %x %x %x %x %x\n", buffer[12], buffer[13], buffer[14],buffer[15],buffer[16],buffer[17]);
	if (0x1C == FU_FLAG)//如果是FU型分割
	{
		//printf("FU_FLAG\n");
		MARK_BIT = (buffer[payloadoffset + 1]) >> 7; //取第二个字节的最高位，以便判断是否是此NALU的最后一包
		if ((p_rtp->i_nalu_ok_flag) == 0)//这是当前NALU的第一包
		{
			if ((recv_bytes - 14) <= 0)
			{
			}
			else
			{
				NAL_HEAD = ((buffer[payloadoffset + 12])&(0xE0)) | ((buffer[payloadoffset + 13])&(0x1F)); //取第13个字节的高3位和第14字节的低5位，拼成此NALU的头
				memset(save_buffer, 0, sizeof (save_buffer));
				save_buffer[3] = 1;
				save_buffer[4] = NAL_HEAD; //将NALU的头保存起来

				memcpy(&(save_buffer[5]), &(buffer[14]), recv_bytes - 14); //从第15字节开始就是NALU的数据部分，保存起来
				save_len = recv_bytes - 9; //减12字节的RTP头，减2字节FU头，加4字节的起始码，加1字节的NALU头
				p_rtp->i_nalu_ok_flag = 1; //这是当前NALU的第一包，接下来的就不是第一包了。
				/*
				NAL_HEAD=((buffer[12])&(0xE0))|((buffer[13])&(0x1F));//取第13个字节的高3位和第14字节的低5位，拼成此NALU的头
				memset(save_buffer,0,sizeof(save_buffer));
				save_buffer[2]=1;
				save_buffer[3]=NAL_HEAD;//将NALU的头保存起来

				memcpy(&(save_buffer[4]),&(buffer[14]),recv_bytes-14);//从第15字节开始就是NALU的数据部分，保存起来
				save_len=recv_bytes-10;//减12字节的RTP头，减2字节FU头，加3字节的起始码，加1字节的NALU头
				p_rtp->i_nalu_ok_flag =1;//这是当前NALU的第一包，接下来的就不是第一包了。
				*/
			}
		}
		else
		{
			memset(save_buffer, 0, sizeof (save_buffer));
			if ((recv_bytes - 14) > 4096)
			{
			}
			else if ((recv_bytes - 14) <= 0)
			{
			}
			else
			{
				memcpy(save_buffer, buffer + 14, recv_bytes - 14); //不是NALU的第一包，直接从第15字节保存起来
				save_len = recv_bytes - 14; //减12字节的RTP头，减2字节FU头
			}
		}
		if (MARK_BIT == 1)//这是此NALU的最后一包
		{
			p_rtp->i_nalu_ok_flag = 0; //这一NALU已经收齐，下面再来的包就是下一个NALU的了
		}
	}
	else if (FU_FLAG == 0x18) // 多个NALU包，组合封包
	{
		//printf("recv_bytes = %d\n", recv_bytes);
		memset(save_buffer, 0, sizeof (save_buffer));
		int i_index = 0;
		int i_src_index = 13;
		short i_len = 0;
		while (1)
		{
			if ((i_src_index) >= recv_bytes)
			{
				break;
			}
			save_buffer[i_index + 0] = 0;
			save_buffer[i_index + 1] = 0;
			save_buffer[i_index + 2] = 0;
			save_buffer[i_index + 3] = 1;
			i_index += 4;
			i_len = (buffer[i_src_index] << 8);
			i_src_index += 1;
			i_len += buffer[i_src_index];
			if ((i_len >= recv_bytes) || (i_len < 0))
			{
				i_index = 0;
				break;
			}

			//printf("i_index = %d, i_len = %d\n", i_index, i_len);
			i_src_index += 1;
			memcpy(save_buffer + i_index, buffer + i_src_index, i_len);
			i_src_index += i_len;
			i_index += i_len;
			/*
			save_buffer[i_index+0] = 0;
			save_buffer[i_index+1] = 0;
			save_buffer[i_index+2] = 1;

			i_index += 3;
			i_len = (buffer[i_src_index] << 8);
			i_src_index += 1;
			i_len += buffer[i_src_index];
			//printf("i_index = %d, i_len = %d\n", i_index, i_len);
			i_src_index += 1;
			memcpy(save_buffer+i_index, buffer+i_src_index, i_len);
			i_src_index += i_len;
			i_index += i_len;
			*/
		}
		save_len = i_index;
		//printf("save_len = %d\n", save_len);
	}
	else if (FU_FLAG == 0x19)
	{
		// TODO
		printf("unkown rtp packet, todo\n");
	}
	else //不是FU型分割，即一个NALU就是一包
	{
		int nalusize = recv_bytes - payloadoffset;
		save_buffer[0] = (nalusize >> 24) & 0xff;
		save_buffer[1] = (nalusize >> 16) & 0xff;
		save_buffer[2] = (nalusize >> 8) & 0xff;
		save_buffer[3] = (nalusize )&0xff;

		memcpy(&(save_buffer[4]), &(buffer[payloadoffset]), recv_bytes - payloadoffset); //第13字节是此NALU的头，14字节及以后是NALU的内容，一起保存
		save_len = recv_bytes - payloadoffset + 4; //减12字节的RTP头

		p_rtp->i_nalu_ok_flag = 0; //一个NALU就是一包，下面再来的包就是下一个NALU的了
		/*
		memset(save_buffer,0,sizeof(save_buffer));
		save_buffer[2]=1;
		memcpy(&(save_buffer[3]),&(buffer[12]),recv_bytes-12);//第13字节是此NALU的头，14字节及以后是NALU的内容，一起保存
		save_len=recv_bytes-12+3;//减12字节的RTP头
		*pnNALUOkFlag=0; //一个NALU就是一包，下面再来的包就是下一个NALU的了
		*/
	}

	return save_len; //save_buffer里面要保存多少字节的数据
}
