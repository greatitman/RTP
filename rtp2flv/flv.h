#ifndef FLV_H
#define FLV_H
#include <sys/types.h>
#include "flv_bytestream.h"
#include "osdep.h"
typedef struct _frame_t
{
	int i_type; // 1-VIDEO, 2-AUDIO
	uint32_t i_frame_size;
	uint8_t* p_frame;
	uint64_t i_time_stamp;
	int i_flag;

	struct _frame_t* p_next;
} frame_t;
typedef struct rtp2flvCtx_t{
	//视频缓冲区
	uint8_t* p_video_frame;
	uint32_t i_video_frame_index;
	uint32_t i_video_time_stamp;
	uint32_t i_video_time_startoffset;
	//音频缓冲区
	uint8_t* p_audio_frame;
	uint32_t i_audio_frame_index;
	uint32_t i_audio_time_stamp;

	uint32_t i_nalu_ok_flag;
	uint32_t i_last_pkt_num;
	uint32_t i_slice_first_nalu;

	uint32_t i_aui_last_pkt_num;

	uint32_t i_exit; // 0-正常 1-退出

	char p_ip[40];
	int i_port;

	void* p_opaque;

	//贞缓冲区,存放完整的音视频数据
	int i_buf_num;
	frame_t* p_frame_buf;
	frame_t* p_frame_header;

	uint32_t i_video_lasttime; // 调整原始流里面的视频时间戳
	uint32_t i_audio_time; // 调整原始流里面的音频时间戳

	uint16_t i_seq_num; // 序列号
	int has_sps;
	int has_pps;
	unsigned int rtptimebase;
	float lastfiletime;
	unsigned int  rtppayload;
	float fps;
}rtp2flvCtx_t;
#define CHECK(x)\
do {\
\
if ((x) < 0)\
	return -1; \
} while (0)
typedef struct rtpbuf_t{
	unsigned char * buf;
	int bufsize;
	int bufpos;
	int bufnextpos;
}rtpbuf_t;
typedef struct x264_nal_t
{
	uint8_t *p_payload;
	int i_payload;
	unsigned int i_type;
}x264_nal_t;

typedef struct
{
	flv_buffer *c;

	uint8_t *sei;
	int sei_len;

	int64_t i_fps_num;
	int64_t i_fps_den;
	int64_t i_framenum;

	uint64_t i_framerate_pos;
	uint64_t i_duration_pos;
	uint64_t i_filesize_pos;
	uint64_t i_bitrate_pos;
	uint64_t i_firsttimestamp_pos;
	uint8_t b_write_length;
	int64_t i_prev_dts;
	int64_t i_prev_cts;
	int64_t i_delay_time;
	int64_t i_init_delta;
	int i_delay_frames;
	float lastfiletime;
	double d_timebase;
	int b_vfr_input;
	int b_dts_compress;
	int64_t i_video_time_startoffset;
	unsigned start;
	float fps;
} flv_hnd_t;

int FillBuffer(char *filename);
int InitFirstPos(rtpbuf_t *rtpReadbuf);
int GetNalFromBuffer(x264_nal_t *nalPacket);

int write_header(flv_buffer *c);
int open_file(char *psz_filename, flv_hnd_t **p_handle);
int set_script_param(flv_hnd_t *handle, double i_width, double i_height);
int write_h264_headers(flv_hnd_t *handle, x264_nal_t *p_nal);
int write_h264_frame(rtp2flvCtx_t *p_rtp,flv_hnd_t *handle, x264_nal_t nalu);
void rewrite_amf_double(FILE *fp, uint64_t position, double value);
int close_file(flv_hnd_t *handle);
//int close_file( flv_hnd_t *handle, int64_t largest_pts, int64_t second_largest_pts );

#endif
